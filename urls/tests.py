import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UrlDataTestCase(APITestCase):
    def setUp(self):
        self.valid_payload = {
            'url': 'https://www.django-rest-framework.org/api-guide/testing/'
        }

    def test_short_url(self):
        url = reverse("short-url")
        response = self.client.post(url, data=json.dumps(self.valid_payload), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        print(response.content)


class UrlDataWithCustomHashTestCase(TestCase):
    def setUp(self):
        self.valid_payload = {
            'url': 'https://newbedev.com/django-test-app-error-got-an-error-creating-the-test-database-permission-denied-to-create-database',
            'hash': "abbbbbnjhijiojh"
        }

    def test_short_url(self):
        url = reverse("short-url")
        response = self.client.post(url, data=json.dumps(self.valid_payload), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class UrlDataWithExpirationTimeTestCase(TestCase):
    def setUp(self):
        self.valid_payload = {
            'url': 'https://www.google.com/',
            'hash': "abbbbbnjhijiojh",
            'expired_at': '2023-09-04 06:00:00',
        }

    def test_short_url(self):
        url = reverse("short-url")
        response = self.client.post(url, data=json.dumps(self.valid_payload), content_type='application/json')
        short_url = response.content.decode("ascii")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        redirect_url = reverse("redirect", kwargs={'hash': "abbbbbnjhijiojh"})
        r_response = self.client.get(redirect_url)
        self.assertRedirects(r_response, 'https://www.google.com/', fetch_redirect_response=False)


class UrlClickedNumberTestCase(TestCase):
    def test_short_url(self):
        url = reverse("count-redirection", kwargs={'hash': "abbbbbnjhijiojh"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UrlDataPrivateTestCase(TestCase):
    def setUp(self):
        self.valid_payload = {
            'url': 'https://quera.ir/',
            'hash': "quuuhgurhgjndfjgn",
            'expired_at': '2023-09-04 06:00:00',
            # 'is_private': 'True'
        }

    def test_short_url(self):
        url = reverse("short-url")
        response = self.client.post(url, data=json.dumps(self.valid_payload), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        redirect_url = reverse("redirect", kwargs={'hash': "quuuhgurhgjndfjgn"})
        print(redirect_url)
        r_response = self.client.get(redirect_url)
        print(r_response)
        self.assertRedirects(r_response, 'https://quera.ir/', fetch_redirect_response=False)
