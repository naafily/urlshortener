import datetime

from django.db import models
from django.utils import timezone

from accounts.models import Member
from .generators import hash_generator

_EXPIRATION_PERIOD = 10


class UrlData(models.Model):
    url = models.CharField(max_length=200, unique=True)
    hash = models.CharField(max_length=20, unique=True, default=hash_generator())
    created_at = models.DateTimeField()
    expired_at = models.DateTimeField(blank=True)
    is_private = models.BooleanField(default=False)
    member = models.ForeignKey(Member, on_delete=models.SET_NULL, default=None, null=True)

    def __str__(self):
        return f'short url for {self.url} is {self.hash}'

    def save(self, *args, **kwargs):
        dt = timezone.timedelta(hours=_EXPIRATION_PERIOD)
        self.created_at = datetime.datetime.now(timezone.utc)
        # self.member =
        if self.expired_at is None:
            self.expired_at = self.created_at + dt
        super(UrlData, self).save(*args, **kwargs)
