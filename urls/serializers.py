from rest_framework import serializers

from .models import UrlData


class UrlSerializer(serializers.ModelSerializer):
    member = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = UrlData
        fields = ['url', 'expired_at', 'hash', 'is_private', 'member']
