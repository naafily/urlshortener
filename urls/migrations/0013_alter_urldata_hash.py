# Generated by Django 3.2.7 on 2021-10-30 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urls', '0012_alter_urldata_hash'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urldata',
            name='hash',
            field=models.CharField(default='W5eGIRlfijXqCydTuCHT', max_length=20, unique=True),
        ),
    ]
