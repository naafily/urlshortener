# Generated by Django 3.2.7 on 2021-10-30 09:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urls', '0010_alter_urldata_hash'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urldata',
            name='hash',
            field=models.CharField(default='kpkJXBlq7WNATuyTkr6O', max_length=20, unique=True),
        ),
    ]
