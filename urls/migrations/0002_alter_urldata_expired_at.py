# Generated by Django 3.2.7 on 2021-10-02 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urls', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='urldata',
            name='expired_at',
            field=models.DateTimeField(blank=True),
        ),
    ]
