from django.urls import path

from .views import ShortUrlsAPIView, RedirectView, CountRedirectionView

urlpatterns = [
    path('', ShortUrlsAPIView.as_view(), name="short-url"),
    path('<str:hash>', RedirectView.as_view(), name="redirect"),
    path('info/<str:hash>', CountRedirectionView.as_view(), name="count-redirection"),
]
