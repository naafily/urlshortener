import datetime
from datetime import timezone

from django.contrib.sites.shortcuts import get_current_site
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from rest_framework import views, status
from rest_framework.response import Response

from .models import Member
from .models import UrlData
from .serializers import UrlSerializer

redis_shortened_url_clicks_counter_fmt = 'clicks:%s:url'
redis_shortened_url_member_id_fmt = 'member_id:%s:url'
redis_shortened_url_is_private_fmt = 'is_private:%s:url'


class ShortUrlsAPIView(views.APIView):
    serializer_class = UrlSerializer

    def post(self, request):
        current_site = get_current_site(request)
        serializer = UrlSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.user.is_authenticated:
            member = Member.objects.filter(user=request.user).first()
            if member is not None:
                url_data = serializer.save(member=member)
        else:
            url_data = serializer.save(is_private=False)

        long_url = "http://{}/{}".format(current_site, url_data.hash)
        return Response(long_url, status=status.HTTP_201_CREATED)


class RedirectView(views.APIView):
    def get(self, request, hash):
        url = self.get_from_cache(request, hash)
        if url is not None:
            return redirect(url)
        else:
            url = self.get_from_db(request, hash)
            if url is not None:
                return redirect(url)

        return Response(status=status.HTTP_404_NOT_FOUND)

    def get_from_cache(self, requset, hash):
        url_data = cache.get(hash)
        if url_data is not None:
            url_owner = url_string_formatter(redis_shortened_url_member_id_fmt, hash)
            url_private = url_string_formatter(redis_shortened_url_is_private_fmt, hash)
            is_private = cache.get(url_private)
            if is_private:
                owner_id = cache.get(url_owner)
                if owner_id == requset.user.id:
                    inc_url_clicks_counter(hash)
                    return url_data
                raise PermissionDenied()
            else:
                inc_url_clicks_counter(hash)
                return url_data
        return None

    def get_from_db(self, request, hash):
        now = datetime.datetime.now(timezone.utc)
        qq = UrlData.objects.all()
        url_data = UrlData.objects.filter(hash=hash, expired_at__gt=now).exclude(is_private=True).first()
        if url_data is not None:
            cache_in_redis(url_data)
            return url_data.url
        elif request.user.is_authenticated:
            url_data = UrlData.objects.filter(hash=hash, expired_at__gt=now, is_private=True).first()
            if url_data is not None:
                if url_data.member.user == request.user:
                    cache_in_redis(url_data)
                    return url_data.url
                raise PermissionDenied
        return None


class CountRedirectionView(views.APIView):
    def get(self, request, hash):
        url_clicks_counter_key = url_string_formatter(redis_shortened_url_clicks_counter_fmt, hash)
        c = cache.get(url_clicks_counter_key)
        return Response(c, status=status.HTTP_200_OK)


def cache_in_redis(url_data):
    now = datetime.datetime.now(timezone.utc)
    delta = url_data.expired_at - now
    cache.set(url_data.hash, url_data.url, delta.seconds)
    url_clicks_counter = url_string_formatter(redis_shortened_url_clicks_counter_fmt, url_data.hash)
    url_owner = url_string_formatter(redis_shortened_url_member_id_fmt, url_data.hash)
    url_private = url_string_formatter(redis_shortened_url_is_private_fmt, url_data.hash)
    if url_data.member_id is not None:
        cache.set(url_owner, url_data.member_id)
    cache.set(url_private, int(url_data.is_private))
    cache.set(url_clicks_counter, 1)


def inc_url_clicks_counter(hash):
    url_clicks_counter = url_string_formatter(redis_shortened_url_clicks_counter_fmt, hash)
    cache.incr(url_clicks_counter)


def url_string_formatter(str_fmt, url):
    return str_fmt % url
