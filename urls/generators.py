import random
import string


def hash_generator():
    s = string.ascii_uppercase + string.ascii_lowercase + string.digits
    hashed_url = ''.join(random.choices(s, k=20))
    return hashed_url
