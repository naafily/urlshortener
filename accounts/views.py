from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Member
from .serializers import MemberSerializer, MemberLoginSerializer


class SignUpView(APIView):
    serializer_class = MemberSerializer

    def post(self, request):
        serialized = MemberSerializer(data=request.data)
        if serialized.is_valid():
            member = serialized.save()
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class LoginAPIView(APIView):
    serializer_class = MemberLoginSerializer

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('user_password')
        user = get_object_or_404(User, username=username)
        obj = get_object_or_404(Member, user=user)
        if user is not None and obj is not None:
            authenticated_user = authenticate(username=user.username, password=password)
            if authenticated_user is not None:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class LogoutAPIView(APIView):
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            return Response('User Logged out successfully')
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
