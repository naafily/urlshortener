from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Member


class MemberSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', write_only=True,
                                     required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)
    user_email = serializers.CharField(source='user.email', write_only=True,
                                       required=True)

    class Meta:
        model = Member
        fields = ('username', 'user_password', 'user_email')

    def create(self, validated_data):
        user = User.objects.create(username=validated_data["user"]["username"], email=validated_data["user"]["email"],
                                   password=make_password(validated_data["user"]['password']))
        member = Member.objects.create(user=user)
        return member


class MemberLoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', write_only=True,
                                    required=True)
    user_password = serializers.CharField(source='user.password', write_only=True,
                                          required=True)

    class Meta:
        model = Member
        fields = ('username', 'user_password')
